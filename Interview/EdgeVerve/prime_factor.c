#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num, i;
	int tmp;
	printf("Enter no to find prime factor : ");
	scanf("%d", &num);
	tmp = num;
	printf("Prime factor of %d : \n", tmp);
	for(i = 2; i < tmp; i++)
	{
		while(num % i == 0)
		{
			num = num / i;
			printf("%d ", i);
		}
	}
	return (0);
}
