#include <stdio.h>
#include <string.h>

// Program to return string Day for given input Day after which for given number of day difference
// Example If I/p is Mon and 2 then O/P = Wed
// Example If I/p is Wed and 23 then O/P = Mon

char * solution(char *S, int K) {
    // write your code in C99 (gcc 6.2.0)
	char *A[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

	int offset = K % 7;
	int j = 0;

	for (int i = 0; i < 7; i++) {
		if (!strcmp(A[i], S)) {
			return A[(i + offset)%7];
		}
	}

}

int main()
{
	char *A[] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
	int k = 23;
	char *result = solution(A[5], k);

	printf("%s\n", result);

	return 0;
}
