#include <stdio.h>

// Program to return max occurance of matching same fraction value for X[k]/Y[k]

int solution(int X[], int Y[], int N) {
	float C[N];

	int i, k=0;

	for (i = 0; i < N; i++) {
			if (Y[i] != 0) {
				C[k++] = (float)X[i]/Y[i];
			}
	}

	int occ = 1;
	int max = 1;
	for (int q = 0; q < k-1; q++) {
		if (max < occ) {
			max = occ;
		}
		occ = 1;
		if (C[q] != 0.000000) {
			for (int w = q+1; w < k; w++) {
				if (C[q] == C[w]) {
					occ++;
					C[w] = 0.000000;
				}
			}
		}
	}

	return max;
}

int main()
{
	//int X[] = {1, 2, 3, 4, 0};
	//int Y[] = {1, 2, 3, 4, 0};
	
	int X[] = {1, 2, 3, 4, 0};
	int Y[] = {2, 3, 6, 8, 4};
	int N = 5;

	int result = solution(X, Y, N);
	printf("Occruance %d\n", result);
	return 0;
}

