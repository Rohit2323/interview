#include <stdio.h>
#include <string.h>

// Program to return length of max concatenated string from given array
// contatenate two or more string from given string array such that concatenated string contains unique element
// For char *A[] = {"co", "dil", "ity"}
// Possible cont "codil", "coity", "dilco", "ityco" so return 5 max length of cont string

#if 0
int solution(char *A[], int N) {
	int H[26] = {0};
	char C[20][100];
	int static l = 0;
    // write your code in C99 (gcc 6.2.0)
	for (int i = 0; i < N; i++) {
		int flag = 0;
		for (int j = i+1; j < N; j++) {
			int k = 0;
			while (A[i][k] != '\0') {
				int q = 0;
				while (A[j][q] != '\0') {
					printf("%c %c\n", A[i][k], A[j][q]);
					if (A[i][k] == A[j][q]) {
						flag = 1;
						printf("11\n");
						break;
					}
					q++;
				}
				k++;
			}
		}
		if (flag == 0) {
			strcpy(C[l], A[i]);
			printf("C[%d] %s\n", l, C[l]);
			l++;
		}
	}

	return 0;
}
#endif
int solution(char *A[], int N) {
	char C[100][100];
	int static c = 0;
	int i, j;

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (i != j) {
				strcpy(C[c], A[i]);
				strcat(C[c], A[j]);
				c++;
			}
		}
	}
	for (i = 0; i < c; i++) {
		printf("%d: %s\n", i, C[i]);
	}

	int max_len = 0;
	for (i = 0; i < c; i++) {
		int k = 0;
		int H[26] = {0};
		while (C[i][k] != '\0') {
			//H[C[i][k-97]]++;
			//printf("%c %d\n", C[i][k], C[i][k]-97);
			if (H[C[i][k]-97] >= 1) {
				break;
			} else {
				H[C[i][k]-97]++;
				//printf("%c %d\n", C[i][k], C[i][k]-97);
			}
			k++;
		}
		if (C[i][k] == '\0') {
			printf("%d: %s\n", i, C[i]);
			if (max_len < strlen(C[i])) {
				max_len = strlen(C[i]);
			}
		}
	}
	

	return max_len;
}

int main()
{
	int result;

	//char *A[] = {"co", "dil", "ity"};
	//result = solution(A, 3);
	
	char *A[] = {"abc", "def", "ghi", "eft"};
	result = solution(A, 4);

	printf("Max conc string lenth %d\n", result);

	return 0;
}
