#include <stdio.h>
#include <stdlib.h>

int max_array(int A[],int  n)
{
    int max = A[0];
    for (int i = 0; i < n; i++) {
        if (A[i] > max) {
          max = A[i];   
        }
    }
    return max;
}

int solution(int A[], int N) {
    int i;
    // write your code in C99 (gcc 6.2.0)
    int max = max_array(A, N);
    int *B = (int *)malloc((max+1) * sizeof(int));
    
    for (i = 0; i < max+1; i++) {
        B[i] = 0;
    }
    
    for (i = 0; i < N; i++) {
        B[A[i]]++;
    }
    
    for (i = 1; i < max+1; i++) {
        if (B[i] == 0) {
            printf("%d\n", i);
            return 0;
        }
    }
    printf("%d\n", i);
    return 0;
    
}

int main()
{
	//int A[] = {1, 3, 4, 6, 1, 2};
	//int A[] = {1, 2, 3};
	int A[] = {-1, -3};

	solution(A, 2);
	return 0;
}
