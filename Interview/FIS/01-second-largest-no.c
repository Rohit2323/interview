#include <stdio.h>

// Program to find 2nd largest no in an array

int large_no(int A[], int n)
{
	int i;
	int max_1 = A[0];
	int max_2 = A[0];

	for (i = 1; i < n; i++) {
		if (A[i] > max_1) {
			max_2 = max_1;
			max_1 = A[i];
		}
	}

	return max_2;
}

int main()
{
	//int A[] = {1, 2, 3, 4, 5};
	int A[] = {1, 4, 3, 2, 5,};

	printf("2nd largest no %d\n", large_no(A, 5));

	return 0;
}
