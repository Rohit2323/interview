#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>

#define PORT 54321

// Commands
#define PWD "pwd"
#define LS "ls"
#define CD "cd"
#define BYE "bye"

int is_valid_command(char *command)
{
	char cmd[4][64] = {PWD, LS, CD, BYE};

	for (int i = 0; i < 4; i++) {
		if (!strncmp(command, cmd[i], strlen(cmd[i]))) {
			return 1;
		}
	}
	return 0;
}

void handle_commands(int sock_fd)
{
	int read_ret;
	char buffer[1024] = {0};

	while (1) {
		printf("cmd: ");
		memset(&buffer, 0, sizeof(buffer));
		fgets(buffer, sizeof(buffer), stdin);
		if (!is_valid_command(buffer)) {
			printf("Enter valid command\n");
			continue;
		}

		send(sock_fd, buffer, strlen(buffer), 0);
		if (!strncmp(buffer, BYE, strlen(BYE))) {
			printf("closing the session\n");
			break;
		}

		struct timeval tv;
		tv.tv_sec = 0;  /* 1 Secs Timeout */
		tv.tv_usec = 50000;
		if(setsockopt(sock_fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(tv)) < 0) {
			printf("Time Out\n");
			return;
		}

		while (1) {
			memset(&buffer, 0, sizeof(buffer));
			read_ret = recv(sock_fd, buffer, sizeof(buffer), 0);
			if (read_ret > 0) {
				buffer[read_ret-1] = '\0';
				printf("%s\n", buffer);
			} else {
				break;
			}
		}
	}
}

int main()
{
	int sock_fd;
	struct sockaddr_in address;

	if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("create socket failed\n");
		exit(EXIT_FAILURE);
	}

	memset(&address, 0, sizeof(address));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr("127.0.0.1"); // localhost
	address.sin_port = htons(PORT);

	if (connect(sock_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		printf("connect failed\n");
		exit(EXIT_FAILURE);
	}

	handle_commands(sock_fd);

	close(sock_fd);

	return 0;
}
