#include <stdio.h>
#include <string.h>
#include <unistd.h> // getcwd()
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <limits.h>
#include <dirent.h> // readdir(), opendir()
#include <sys/time.h> // FD_SET, FD_ISSET, FD_ZERO macors
#include <errno.h>
#include <arpa/inet.h>

#define PORT 54321

// Commands
#define PWD "pwd"
#define LS "ls"
#define CD "cd"
#define BYE "bye"

/* Program to implement tcp directory listing server which will handle below commands
	1. pwd : display server current directory under the session 

	2. ls: list files/dirs in current server directory, one line per file/dir. The display format will be: 
		<type> <FileOrDirName>
		type: "FILE" or "DIR"
		FileOrDirName: name of the file/dir 

	3. cd <directory> : enter the directory 

	4. bye: close the connection session
 */

#define SIZE 1024

void pwd(int sd, int port)
{
	char buffer[SIZE] = {0};
	char cwd[PATH_MAX];
	
	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		snprintf(buffer, SIZE, "%s\n", cwd);
		send(sd, buffer, strlen(buffer), MSG_NOSIGNAL);
		printf("sent(sock#%d port#%d) :\n%s\n", sd, port, buffer);
	} else {
		printf("pwd() error\n");
	}
}

void list_dir_content(char *path, int sd, int port)
{
	char buffer[SIZE] = {0};
	DIR *d;
	struct dirent *dir;
	int ret;
	int off = 0;

	d = opendir(path);
	if (d) {
		ret = 0;
		while ((dir = readdir(d)) != NULL) {
			off += ret;
			if (off <= SIZE) {
				send(sd, buffer, strlen(buffer), 0);
				printf("sent(sock#%d port#%d) :\n%s\n", sd, port, buffer);
				memset(&buffer, 0, sizeof(buffer));
				off = 0;
			}
			if (dir->d_type == DT_DIR) {
				ret = snprintf(buffer+off, SIZE, "DIR %s\n", dir->d_name);
			} else {
				ret = snprintf(buffer+off, SIZE, "FILE %s\n", dir->d_name);
			}
		}
		closedir(d);
		if (ret > 0) {
			send(sd, buffer, strlen(buffer), 0);
			printf("sent(sock#%d port#%d) :\n%s\n", sd, port, buffer);
		}
	}
}

void change_dir(char *path, int sd, int port)
{
	char buffer[SIZE] = {0};

	if (chdir(path) < 0) {
		printf("Failed to change dir\n");
		snprintf(buffer, SIZE, "Invalid path\n");
		send(sd, buffer, strlen(buffer), MSG_NOSIGNAL);
		printf("sent(sock#%d port#%d) :\n%s\n", sd, port, buffer);
		return;
	} else {
		pwd(sd, port);
	}
}

void handle_commands(int sd, int port)
{
	char buffer[SIZE] = {0};
	char *token = NULL;
	int len;

	while (1) {
		// Check if it was  for closing, and also for read the incoming message
		memset(&buffer, 0, sizeof(buffer));
		if (read(sd, buffer, sizeof(buffer)) == 0) {
			// somebody disconnected, get his details
			printf("Host disconnected, port %d\n", port);

			// close the socket and mark as 0 in list for reuse
			close(sd);
			break;
		} else {
			if (!strncmp(buffer, PWD, strlen(PWD))) {
				pwd(sd, port);
			} else if (!strncmp(buffer, LS, strlen(LS))) {
				list_dir_content(".", sd, port);
			} else if (!strncmp(buffer, CD, strlen(CD))) {
				token = strtok(buffer, " ");
				if (token) {
					token = strtok(NULL, " ");
					if (token) {
						len = strlen(token);
						if (token[len-1] == '\n') {
							token[len-1] = '\0';
						}
						change_dir(token, sd, port);
					}
				}
			} else if (!strncmp(buffer, BYE, strlen(BYE))) {
				printf("Host disconnected, port %d\n", port);
				close(sd);
				break;
			} else {
				printf("Invalid command\n");
				break;
			}
		}
	}
}

int main()
{
	int master_socket, new_socket;
	struct sockaddr_in address;
	int opt = 1;
	int addr_len;
	int pid;

	// create a master socket
	if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("create socket failed\n");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) < 0) {
		printf("setsockopt failed\n");
		exit(EXIT_FAILURE);
	}

	memset(&address, 0, sizeof(address));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(PORT);

	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0) {
		printf("bind failed\n");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d\n", PORT);

	// maximum 5 pending connections for master socket
	if (listen(master_socket, 5) < 0) {
		printf("listen failed\n");
		exit(EXIT_FAILURE);
	}
	
	addr_len = sizeof(address);
	printf("waiting for connections\n");

	while (1) {
		if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addr_len)) < 0) {
			printf("accept failed\n");
			exit(EXIT_FAILURE);
		}

		printf("New connection, sock fd is %d, port : %d\n", new_socket, ntohs(address.sin_port));

		pid = fork();
		if (pid == 0) {
			getpeername(new_socket, (struct sockaddr *)&address, (socklen_t *)&addr_len);
			handle_commands(new_socket, ntohs(address.sin_port));

		} else {
			close(new_socket);
		}
	}

	close(master_socket);

	return 0;
}

