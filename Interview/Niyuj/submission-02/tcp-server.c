#include <stdio.h>
#include <string.h>
#include <unistd.h> // getcwd()
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <limits.h>
#include <dirent.h> // readdir(), opendir()
#include <sys/time.h> // FD_SET, FD_ISSET, FD_ZERO macors
#include <errno.h>
#include <arpa/inet.h>

#define PORT 54321

// Commands
#define PWD "pwd"
#define LS "ls"
#define CD "cd"
#define BYE "bye"

/* Program to implement tcp directory listing server which will handle below commands
	1. pwd : display server current directory under the session 

	2. ls: list files/dirs in current server directory, one line per file/dir. The display format will be: 
		<type> <FileOrDirName>
		type: "FILE" or "DIR"
		FileOrDirName: name of the file/dir 

	3. cd <directory> : enter the directory 

	4. bye: close the connection session
 */

char buffer[2048] = {0};

void pwd()
{
	char cwd[PATH_MAX];
	
	memset(&buffer, 0, sizeof(buffer));

	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		sprintf(buffer, "%s", cwd);
	} else {
		printf("pwd() error\n");
	}
}

void list_dir_content(char *path)
{
	DIR *d;
	struct dirent *dir;
	int ret;
	int off = 0;

	memset(&buffer, 0, sizeof(buffer));
	
	d = opendir(path);
	if (d) {
		ret = 0;
		while ((dir = readdir(d)) != NULL) {
			off += ret;
			if (dir->d_type == DT_DIR) {
				ret = sprintf(buffer+off, "DIR %s\n", dir->d_name);
			} else {
				ret = sprintf(buffer+off, "FILE %s\n", dir->d_name);
			}
		}
		closedir(d);
	}
}

void change_dir(char *path)
{
	if (chdir(path) < 0) {
		memset(&buffer, 0, sizeof(buffer));
		printf("Failed to change dir\n");
		sprintf(buffer, "Invalid path\n");
		return;
	} else {
		pwd();
	}
}

void handle_commands(int sd, int port, int client_socket[], int index)
{
	char *token = NULL;
	int len;
	
	// Check if it was  for closing, and also for read the incoming message
	memset(&buffer, 0, sizeof(buffer));
	if (read(sd, buffer, sizeof(buffer)) == 0) {
		// somebody disconnected, get his details
		printf("Host disconnected, port %d\n", port);

		// close the socket and mark as 0 in list for reuse
		close(sd);
		client_socket[index] = 0;
	} else {
		if (!strncmp(buffer, PWD, strlen(PWD))) {
			pwd();
		} else if (!strncmp(buffer, LS, strlen(LS))) {
			list_dir_content(".");
		} else if (!strncmp(buffer, CD, strlen(CD))) {
			token = strtok(buffer, " ");
			if (token) {
				token = strtok(NULL, " ");
				if (token) {
					len = strlen(token);
					if (token[len-1] == '\n') {
						token[len-1] = '\0';
					}
					change_dir(token);
				}
			}
		} else if (!strncmp(buffer, BYE, strlen(BYE))) {
			printf("Host disconnected, port %d\n", port);
			close(sd);
			client_socket[index] = 0;
			return;
		} else {
			printf("Invalid command\n");
			return;
		}

		send(sd, buffer, strlen(buffer), 0);
		printf("sent(sock#%d port#%d) :\n%s\n", sd, port, buffer);
	}
}

int main()
{
	int master_socket, new_socket, client_socket[128], max_clients = 128;
	int activity, i, max_sd, sd;
	struct sockaddr_in address;
	int opt = 1;
	int addr_len;
	// set of socket descriptors
	fd_set readfds;

	for (i = 0; i < max_clients; i++) {
		client_socket[i] = 0;
	}

	// create a master socket
	if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("create socket failed\n");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) < 0) {
		printf("setsockopt failed\n");
		exit(EXIT_FAILURE);
	}

	memset(&address, 0, sizeof(address));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(PORT);

	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0) {
		printf("bind failed\n");
		exit(EXIT_FAILURE);
	}
	printf("Listener on port %d\n", PORT);

	// maximum 5 pending connections for master socket
	if (listen(master_socket, 5) < 0) {
		printf("listen failed\n");
		exit(EXIT_FAILURE);
	}
	
	addr_len = sizeof(address);
	printf("waiting for connections\n");

	while (1) {
		// clear the socket set
		FD_ZERO(&readfds);

		// add master socket to set
		FD_SET(master_socket, &readfds);
		max_sd = master_socket;

		// add child sockets to set
		for (i = 0; i < max_clients; i++) {
			// socket descriptor
			sd = client_socket[i];

			// if valid socket descriptor then add to read list
			if (sd > 0) {
				FD_SET(sd, &readfds);
			}

			// highest file descriptor number, need if for select()
			if (sd > max_sd) {
				max_sd = sd;
			}
		}

		// wait for an activity on one of the socket
		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
		if ((activity < 0) && (errno != EINTR)) {
			printf("select error\n");
		}

		// If something happened on the master socket then its an incoming connection
		if (FD_ISSET(master_socket, &readfds)) {
			if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addr_len)) < 0) {
				printf("accept failed\n");
				exit(EXIT_FAILURE);
			}

			printf("New connection, sock fd is %d, port : %d\n", new_socket, ntohs(address.sin_port));

			// add new socket to array of socket
			for (i = 0; i < max_clients; i++) {
				if (client_socket[i] == 0) {
					client_socket[i] = new_socket;
					printf("Adding list of socket as %d\n", i);
					break;
				}
			}
		}

		// else its some IO operations on some other socket
		for (i = 0; i < max_clients; i++) {
			sd = client_socket[i];

			if (FD_ISSET(sd, &readfds)) {
				getpeername(sd, (struct sockaddr *)&address, (socklen_t *)&addr_len);
				handle_commands(sd, ntohs(address.sin_port), client_socket, i);
			}
		}
	}

	close(master_socket);

	return 0;
}

