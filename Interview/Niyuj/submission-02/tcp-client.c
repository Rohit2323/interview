#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define PORT 54321

// Commands
#define PWD "pwd"
#define LS "ls"
#define CD "cd"
#define BYE "bye"

char buffer[2048] = {0};
int client_exit = 0;

int is_valid_command()
{
	char cmd[4][64] = {PWD, LS, CD, BYE};

	for (int i = 0; i < 4; i++) {
		if (!strncmp(buffer, cmd[i], strlen(cmd[i]))) {
			return 1;
		}
	}
	return 0;
}

void handle_commands(int sock_fd)
{
	int read_ret;

	while (1) {
		printf("cmd: ");
		memset(&buffer, 0, sizeof(buffer));
		fgets(buffer, sizeof(buffer), stdin);
		if (!is_valid_command()) {
			printf("Enter valid command\n");
			continue;
		}

		send(sock_fd, buffer, strlen(buffer), 0);
		if (!strncmp(buffer, BYE, strlen(BYE))) {
			printf("closing the session\n");
			break;
		}

		memset(&buffer, 0, sizeof(buffer));
		read_ret = read(sock_fd, buffer, sizeof(buffer));
		printf("%s\n", buffer);
	}
}

int main()
{
	int sock_fd;
	struct sockaddr_in address;

	if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("create socket failed\n");
		exit(EXIT_FAILURE);
	}

	memset(&address, 0, sizeof(address));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr("127.0.0.1"); // localhost
	address.sin_port = htons(PORT);

	if (connect(sock_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		printf("connect failed\n");
		exit(EXIT_FAILURE);
	}

	handle_commands(sock_fd);

	close(sock_fd);

	return 0;
}
