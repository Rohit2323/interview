#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Write a function which will copy string in str.

void foo(char **dest)
{
	char *src = "hello World!";
	int size = strlen(src)+1;

	*dest = (char *)malloc(size);
	snprintf(*dest, size, "%s", src);
}

int main()
{
	char *str;

	foo(&str);

	printf("str : %s %ld\n",str, strlen(str));

	free(str);

	return 0;
}
