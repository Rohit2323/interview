#include <stdio.h>

#define SIZEOF(n) ((char *)(&n+1) - (char *)&n)

int main()
{
	int a;
	char ch;
	double d;
	short int s;

	printf("int size = %d\n", SIZEOF(a));
	printf("char size = %d\n", SIZEOF(ch));
	printf("double size = %d\n", SIZEOF(d));
	printf("short size = %d\n", SIZEOF(s));
}
