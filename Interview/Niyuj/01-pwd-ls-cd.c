#include <stdio.h>
#include <string.h>
#include <unistd.h> // getcwd()
#include <limits.h>
#include <dirent.h> // readdir()

// Program to implement commands pwd, ls, cd
char buffer[2048];

void pwd()
{
	char cwd[PATH_MAX];

	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		printf("Current working directory: %s\n", cwd);
	} else {
		printf("pwd() error\n");
	}
}

void list_dir_content(char *path)
{
	DIR *d;
	struct dirent *dir;
	int ret;
	int off;

	memset(&buffer, 0, sizeof(buffer));

	d = opendir(path);
	if (d) {
		ret = 0;
		while ((dir = readdir(d)) != NULL) {
			off += ret;
			printf("%d\n", ret);
			if (dir->d_type == DT_DIR) {
				printf("DIR %s\n", dir->d_name);
				ret = sprintf(buffer+off, "DIR %s\n", dir->d_name);
			} else {
				printf("FILE %s\n", dir->d_name);
				ret = sprintf(buffer+off, "FILE %s\n", dir->d_name);
			}
		}
	}
}

void change_dir(char *path)
{
	if (chdir(path) < 0) {
		printf("Failed to change dir\n");
	}
}

int main()
{
	char path[PATH_MAX] = ".";
	
	pwd();
	
	list_dir_content(path);
	printf("====================================\n");
	printf("Buffer: %s", buffer);

	chdir("/home");
	pwd();
	
	list_dir_content(path);

	return 0;
}
