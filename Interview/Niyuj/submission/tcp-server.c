#include <stdio.h>
#include <string.h>
#include <unistd.h> // getcwd()
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <signal.h>
#include <limits.h>
#include <dirent.h> // readdir(), opendir()

#define PORT 54321

// Commands
#define PWD "pwd"
#define LS "ls"
#define CD "cd"
#define BYE "bye"

/* Program to implement tcp directory listing server which will handle below commands
	1. pwd : display server current directory under the session 

	2. ls: list files/dirs in current server directory, one line per file/dir. The display format will be: 
		<type> <FileOrDirName>
		type: "FILE" or "DIR"
		FileOrDirName: name of the file/dir 

	3. cd <directory> : enter the directory 

	4. bye: close the connection session
 */

char buffer[2048] = {0};

void pwd()
{
	char cwd[PATH_MAX];
	
	memset(&buffer, 0, sizeof(buffer));

	if (getcwd(cwd, sizeof(cwd)) != NULL) {
		sprintf(buffer, "%s", cwd);
	} else {
		printf("pwd() error\n");
	}
}

void list_dir_content(char *path)
{
	DIR *d;
	struct dirent *dir;
	int ret;
	int off = 0;

	memset(&buffer, 0, sizeof(buffer));
	
	d = opendir(path);
	if (d) {
		ret = 0;
		while ((dir = readdir(d)) != NULL) {
			off += ret;
			if (dir->d_type == DT_DIR) {
				ret = sprintf(buffer+off, "DIR %s\n", dir->d_name);
			} else {
				ret = sprintf(buffer+off, "FILE %s\n", dir->d_name);
			}
		}
		closedir(d);
	}
}

void change_dir(char *path)
{
	if (chdir(path) < 0) {
		memset(&buffer, 0, sizeof(buffer));
		printf("Failed to change dir\n");
		sprintf(buffer, "Invalid path\n");
		return;
	} else {
		pwd();
	}
}

void handle_commands(int sock_fd)
{
	int read_ret;
	char *token = NULL;
	int len;

	while (1) {
		memset(&buffer, 0, sizeof(buffer));
		read_ret = read(sock_fd, buffer, sizeof(buffer));
		if (read_ret) {
			printf("recv : %s\n", buffer);
		}

		if (!strncmp(buffer, PWD, strlen(PWD))) {
			pwd();
		} else if (!strncmp(buffer, LS, strlen(LS))) {
			list_dir_content(".");
		} else if (!strncmp(buffer, CD, strlen(CD))) {
			token = strtok(buffer, " ");
			if (token) {
				token = strtok(NULL, " ");
				if (token) {
					len = strlen(token);
					if (token[len-1] == '\n') {
						token[len-1] = '\0';
					}
					change_dir(token);
				}
			}
		} else if (!strncmp(buffer, BYE, strlen(BYE))) {
			printf("session closed by client\n");
			break;
		} else {
			printf("Invalid command\n");
			continue;
		}

		send(sock_fd, buffer, strlen(buffer), 0);
		printf("sent :\n%s\n", buffer);
	}
}

int main()
{
	int sock_fd, new_socket;
	struct sockaddr_in address;
	int opt = 1;
	int addr_len = sizeof(address);

	if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("create socket failed\n");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		printf("setsockopt failed\n");
		exit(EXIT_FAILURE);
	}

	memset(&address, 0, sizeof(address));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(PORT);

	if (bind(sock_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		printf("bind failed\n");
		exit(EXIT_FAILURE);
	}

	if (listen(sock_fd, 5) < 0) {
		printf("listen failed\n");
		exit(EXIT_FAILURE);
	}

	while (1) {
		if ((new_socket = accept(sock_fd, (struct sockaddr *)&address, (socklen_t *)&addr_len)) < 0) {
			printf("accept failed\n");
			exit(EXIT_FAILURE);
		}

		handle_commands(new_socket);
	}

	close(sock_fd);

	return 0;
}

