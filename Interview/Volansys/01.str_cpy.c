#include <stdio.h>

void str_cpy(char *dest, char *src)
{
	int i;
	for (i = 0; src[i] != '\0'; i++) {
		dest[i] = src[i];
	}
	dest[i] = '\0';
}

int main()
{
	char *str1 = "Hello World";
	char str2[255];

	str_cpy(str2, str1);
	printf("%s\n", str2);
	return 0;
}
