#include <stdio.h>

// Program to check whether given number is perfect square or not

#if 0 // method1
int is_perfect_square(int n)
{
	float i;
	float rem;

	for (i = 1.0; i <= n/2; i++)
	{
		rem = n/i;
		if (rem == i)
		{
			return 1; // true
		}
	}
	return 0; // false
}

#else // method2
int is_perfect_square(int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		if (n == i * i) {
			return 1;
		} else if ( n < i * i) {
			return 0;
		}
	}
	return 0; // false
}
#endif

int main()
{
	int no;
	
	no = 4;
	if (is_perfect_square(no)) {
		printf("%d is perfect square\n", no);
	} else {
		printf("%d is not a perfect square\n", no);
	}
	
	no = 625;
	if (is_perfect_square(no)) {
		printf("%d is perfect square\n", no);
	} else {
		printf("%d is not a perfect square\n", no);
	}
	
	no = 17;
	if (is_perfect_square(no)) {
		printf("%d is perfect square\n", no);
	} else {
		printf("%d is not a perfect square\n", no);
	}

}
