#include <stdio.h>

// Whether following programs works,compiles properly if yes then what is output

/* During compilation compiler will throw below warnings
01-code-snippet.c: In function ‘func’:
01-code-snippet.c:10:9: warning: return discards ‘const’ qualifier from pointer target type [-Wdiscarded-qualifiers]
  return &i;
         ^~
01-code-snippet.c:10:9: warning: function returns address of local variable [-Wreturn-local-addr]
01-code-snippet.c: In function ‘main’:
01-code-snippet.c:15:20: warning: format ‘%d’ expects argument of type ‘int’, but argument 2 has type ‘int *’ [-Wformat=]
  printf("result : %d\n", func());

  o/p - 0

*/
int* func()
{
	const int i = 5;
	int b = 5;
	b = i;
	return &i;
}

int main()
{
	printf("result : %d\n", func());
	return 0;
}
