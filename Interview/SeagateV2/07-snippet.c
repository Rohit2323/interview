#include <stdio.h>

// What will be o/p of below program

/*
	Compiler throws below error
	error: increment of read-only location ‘*p’
*/

int main()
{
	int const *p = 5;

	printf("%d\n", ++(*p));

	return 0;
}
