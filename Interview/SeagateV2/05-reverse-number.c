#include <stdio.h>

// Program to reverse the number

int reverse_no(int n)
{
	int rem;
	int result = 0;

	while (n > 0) {
		rem = n % 10;
		result = result * 10 + rem;
		n = n / 10;
	}
	return result;
}

int main()
{
	int no = 12345;

	printf("%d Reverse number = %d\n", no, reverse_no(no));

	return 0;
}
