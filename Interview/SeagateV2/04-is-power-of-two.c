#include <stdio.h>

// Write a program to check whether given number is in power of 2 or not
/*
 number = 10; o/p- Not power of 2
 number = 8; o/p- Not power of 2
*/

int is_power_of_two(int n)
{
	int i = 2;
	int rem;

	while (n > 2) {
		rem = n % 2;
		if (rem != 0) {
			return 0;
		}
		n = n / 2;
	}
	return 1;
}

int main()
{
	int number;
	int result;

	number = 12;
	result = is_power_of_two(number);
	if (!result) {
		printf("%d is in not power of 2\n", number);
	} else {
		printf("%d is in power of 2\n", number);
	}
	
	number = 1024;
	result = is_power_of_two(number);
	if (!result) {
		printf("%d is in not power of 2\n", number);
	} else {
		printf("%d is in power of 2\n", number);
	}

	return 0;
}
