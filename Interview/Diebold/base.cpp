#include <iostream>
using namespace std;

class A
{
	int a, b;

	public:
	void fun1()
	{
	};
};

class B: public A
{
	int a, b, c;

	public:
	void fun2(){};
	void fun3(){};
};

int main()
{
	B b;
	A a;
	a = b;
	a.fun2();
	return (0);
}
