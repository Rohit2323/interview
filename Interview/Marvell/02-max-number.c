#include <stdio.h>

// Program to find out max number in given 3 numbers with less comparisons

int main()
{
	int A, B, C, largest;

	printf("Enter 3 numbers: ");
	scanf("%d %d %d", &A, &B, &C);

	largest = A > B ? (A > C ? A : C) : (B > C ? B : C);

	printf("%d %d %d largerst is %d\n", A, B, C, largest);

	return 0;
}
