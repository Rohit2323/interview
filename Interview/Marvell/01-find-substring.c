#include <stdio.h>

// Program to find out substring in given string.

void find_sub(char *src, char *sub)
{
	int flag = 0;
	int once = 0;

	for (int i = 0; src[i] != '\0'; i++) {
		flag = 0;
		for (int j = 0; j < sub[j] != '\0'; j++) { // i = 2 for 1st B
			if (src[i+j] != sub[j]) {
				flag = 0;
				break;
			} else {
				flag = 1;
			}
		}
		if (flag == 1) {
			printf("substring found %s\n", src+i);
			once = 1;
		}
	}
	if (!once) {
		printf("substring not found\n");
	}
}

int main()
{
	char *src = "AABBCCDDABBCCDDE";
	char *sub = "ABBC";

	find_sub(src, sub);

	return 0;
}
