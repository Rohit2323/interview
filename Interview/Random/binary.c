#include <stdio.h>

int main()
{
	int num, rem, result = 0;
	int i = 1;

	num  = 10;
	while(num != 0)
	{
	rem = num % 2;
	result = result + rem * i;
	num = num / 2;
	i = i * 10;
	}
	printf("%d\n", result);
	return (0);
}
