#include <stdio.h>

typedef struct list
{
	int data;
	struct list *next;
}node_t;

node_t *head = NULL;
node_t *current = NULL;

node_t* create_node(int value)
{
	node_t *new_node;
	new_node = (node_t*)malloc(sizeof(node_t));
	if(new_node == NULL)
	{
		printf("Failed to create new node\n");
	}
	new_node->data = value;
	new_node->next = NULL;

	return new_node;
}

void display_list()
{
	node_t *trav = head;
	while(trav != NULL)
	{
		printf("%d->", trav->data);
		trav = trav->next;
	}
	printf("NULL\n");
}

void add_last(node_t *new_node)
{
	if(head == NULL)
	{
		head = new_node;
		current = new_node;
	}
	else
	{
		current->next = new_node;
		current = new_node;
	}
}

int main()
{
	system("clear");
	int ch, n;
		
	printf("1: Add_last\n2: Display\n5: Exit\n");
	do
	{
		printf("Enter your choice : ");
		scanf("%d", &ch);
		switch(ch)
		{
			case 1: 
				printf("Enter no to add to list : ");
				scanf("%d", &n);
				add_last(create_node(n));
				break;

			case 2:
				printf("Display Link list : ");
				display_list();
				break;

			default :
				break;
		}

	}while(ch != 5);
	return (0);
}
