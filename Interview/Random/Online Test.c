/* -2 -3 4 -1 -2 1 5 -3
op = [4 -1 -2 1 5] Max sum of sub array Sum = 7*/
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#define MAX_SIZE 8
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int i, j, k, l;
    int sum = 0;
    int sum1 = 0;
    int tmp;
    int array[MAX_SIZE];
    int a[20];
    int cnt = 0;
    printf("Enter the elements : ");
    for(i = 0; i < MAX_SIZE; i++)
    {
        scanf("%d", &array[i]);
    }
    for(i = 0; i < MAX_SIZE; i++)
    {
        for(j = i+1; j < MAX_SIZE; j++)
        {
            if( j == i+1)
            {
                sum = array[i] + array[j];    
                sum1 = sum;
            }
            else
                sum = sum + array[j];
            if(sum > sum1)
            {
                a[cnt] = sum;
                cnt++;
            }
            else
                sum1 = sum;
        }

    }
    k = 0;
    for(l = 0; l < cnt - 1 -k; l++)
    {
        if(a[l] > a[l+1])
        {
            tmp = a[l];
            a[l] = a[l+1];
            a[l+1] = tmp;
        }
    }
    printf("Max Sum of sub array = %d\n", a[l]);
    
    return 0;
}
