#include <stdio.h>

int main()
{
	//char *p = "abcde"; // SIGSEGV
	char p[] = "abcde"; // a, b, k, d, e

	p[2] = 'k';

	for (int i = 0; i < 5; i++) {
		printf("%c\n", p[i]);
	}

	return 0;
}
