#include <stdio.h>
#include <stdlib.h>

// Program to count sum of numbers from given string

int count_sum(char *str)
{
	int sum = 0;
	int i;
	char digit_str[64];
	int j = 0;
	int digit_flag = 0;

	for (i = 0; i < str[i] != '\0'; i++) {
		if (str[i] >= '0' && str[i] <= '9') {
			digit_str[j++] = str[i];
			digit_flag = 1;
		} else {
			if (digit_flag) {
				digit_str[j] = '\0';
				sum = sum + atoi(digit_str);
				j = 0;
				digit_flag = 0;
			}
		}
	}
	if (digit_flag) {
		digit_str[j] = '\0';
		sum = sum + atoi(digit_str);
		j = 0;
		digit_flag = 0;
	}
	return sum;
}

int main()
{
	//char *str = "ab25cd20abcd10qwer11qwert";
	char *str = "9ab25cd20abcd10qwer11qwert";

	printf("count sum od numbers : %d\n", count_sum(str));

	return 0;
}
