#include <stdio.h>
#include <string.h>

// Program to count occurances of given substring into a string

int occurances(char *src, char *sub)
{
	char *token = NULL;
	int result = 0;

	token = src;
	int sub_len = 0;
	int len = strlen(sub);

	while (token = strstr(token+sub_len, sub)) {
		sub_len = len;
		result++;
	}
	return result;
}

int main()
{
	char *src = "abcdtelephone asdwer telephone sdf hfgfdstelephone qwert";
	char *sub = "telephone";

	printf("occurances %d\n", occurances(src, sub));
	return 0;
}
