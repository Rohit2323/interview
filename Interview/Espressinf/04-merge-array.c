#include <stdio.h>

// Program to merge 2 sorted array into 3td sorted array

void merge(int A[], int B[], int m, int n)
{
	int C[m+n];
	int i = 0, j = 0, k = 0;

	while (i < m && j < n) {
		if (A[i] == B[j]) {
			C[k++] = A[i++];
			j++;
		} else if (A[i] < B[j]) {
			C[k++] = A[i++];
		} else {
			C[k++] = B[j++];
		}
	}
	for (; i < m; i++) {
		C[k++] = A[i];
	}
	for (; j < n; j++) {
		C[k++] = B[j];
	}

	printf("Merged array\n");
	for (i = 0; i < k; i++) {
		printf("%d ", C[i]);
	}
	printf("\n");
}

int main()
{
	int A[] = {1, 2, 4, 5, 6};
	int B[] = {1, 2, 55, 88, 99};

	merge(A, B, 5, 5);
	return 0;
}
