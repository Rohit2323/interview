#include <stdio.h>

// Program to print whole array element from given index
// Example: A[] = {1, 2, 3, 4, 5}; given index = 3 o/p = 4, 5, 1, 2, 3
// Handle any given index without throwing error

void print_elements(int A[], int n, int index)
{
	int i = 0;

	int j = index % n;
	while (i < n) {
		if (j >= n) {
			j = j % n;
		}
		printf("%d ", A[j]);
		j++;
		i++;
	}
	printf("\n");
}

int main()
{
	int A[] = {1, 2, 3, 4, 5};
	int n = 5;

	print_elements(A, 5, 103);

	return 0;
}
