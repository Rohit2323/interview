/* C ECHO client example using sockets */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>		// strlen
#include <sys/socket.h>	// socket
#include <arpa/inet.h> 	// inet_addr

int main(int argc, char *argv[])
{
	int sock, n;
	struct sockaddr_in server;
	char message[2000], server_reply[2000];

	// Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == -1)
	{
		printf("Could not create socket\n");
	}
	puts("Socket created");

	server.sin_addr.s_addr = inet_addr("127.0.0.1");
	server.sin_family = AF_INET;
	server.sin_port = htons(8888);

	// Connect to remote server
	if(connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
	{
		perror("conncet failed. Error");
		return 1;
	}
	puts("Connected\n");

	// keep communicating with server
	while(1)
	{
		printf("Enter message : ");
		scanf("%s", message);

		// Send some data
		/*
		if(send(sock, message, strlen(message), 0) < 0)
		{
			puts("Send failed");
			return 1;
		}
		*/

		n = write(sock, message, strlen(message));
		if(n < 0)
		{
			puts("Error writing to socket");
			exit(1);
		}
		puts(message);
		// Receive a reply from server
		/*
		if(recv(sock, server_reply, 2000, 0) < 0)
		{
			puts("recv failed");
			break;
		}
		*/
		bzero(server_reply, 2000);
		n = read(sock, server_reply, 2000);
		if(n < 0)
		{
			puts("Error reading to socket");
			exit(1);
		}


		puts("Server reply: ");
		puts(server_reply);

	}

	close(sock);

	return (0);
}



































































