#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *even(void *param);
void *odd(void *param);

int main()
{
	pthread_t ti1, ti2;
	int iret1, iret2;

	iret1 = pthread_create(&ti1, NULL, even, NULL);
	if(iret1)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret1);
		exit(EXIT_FAILURE);
	}
	
	iret2 = pthread_create(&ti2, NULL, odd, NULL);
	if(iret2)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret2);
		exit(EXIT_FAILURE);
	}

	pthread_join(ti1, NULL);
	pthread_join(ti2, NULL);

	return(0);
}

void *even(void *param)
{
	int i;
	for(i = 2; i < 100; i++)
	{
		if(i%2 == 0)
		{
			printf("E : %d\n", i);
		}
	}
}

void *odd(void *param)
{
	int i;
	for(i = 2; i < 100; i++)
	{
		if(i%2 != 0)
		{
			printf("O : %d\n", i);
		}
	}
}
