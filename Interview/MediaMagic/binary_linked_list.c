/* Convert data binary bir data of link into decimal equivalent
 * exa. 1->0->1->0->NULL then output = 10
 */
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef struct list
{
	int data;
	struct list *next;
}node_t;

node_t *head = NULL;
node_t *current = NULL;

node_t* create_node(int value)
{
	node_t *new_node;
	new_node = (node_t*)malloc(sizeof(node_t));
	if(new_node == NULL)
	{
		printf("Failed to create new node\n");
	}
	new_node->data = value;
	new_node->next = NULL;

	return new_node;
}

void display_list()
{
	node_t *trav = head;
	while(trav != NULL)
	{
		printf("%d->", trav->data);
		trav = trav->next;
	}
	printf("NULL\n");
}

void add_last(node_t *new_node)
{
	if(head == NULL)
	{
		head = new_node;
		current = new_node;
	}
	else
	{
		current->next = new_node;
		current = new_node;
	}
}

void convert_to_decimal()
{
	node_t *trav = head;
	int binary = 0; 
	int decimalNumber = 0, reminder, i = 0;
	while(trav != NULL)
	{
		binary = binary * 10 + trav->data;
		trav = trav->next;
	}
	printf("Binary : %d\n", binary);
	while(binary != 0)
	{
		reminder = binary % 10;
		binary /= 10;
		decimalNumber += reminder * pow(2, i);
		i++;
	}
	printf("Binary Link List to Decimal Link List : %d\n", decimalNumber);
}

int main()
{
	system("clear");
	int ch, n;
		
	printf("1: Add_last\n2: Display\n3: Convert_to_decimal\n4: Exit\n");
	do
	{
		printf("Enter your choice : ");
		scanf("%d", &ch);
		switch(ch)
		{
			case 1: 
				printf("Enter single digit no either 0 or 1 to add to list : ");
				scanf("%d", &n);
				if((n != 0) && (n != 1))
				{
					printf("Enter valid single digit binary bit\n");
					printf("Enter single digit no either 0 or 1 to add to list : ");
					scanf("%d", &n);
				}
				add_last(create_node(n));
				break;

			case 2:
				printf("Display Link list : ");
				display_list();
				break;

			case 3:
				convert_to_decimal();
				break;

			default :
				break;
		}

	}while(ch != 4);
	return (0);
}
