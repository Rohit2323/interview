/* Program to create generic linked list */

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	void *data;
	struct node *next;
}node_t;

void push(node_t **head_ref, void *new_data, size_t data_size)
{
	// Allocate memory for node
	node_t *new_node = (node_t*)malloc(sizeof(node_t));

	new_node->data = malloc(data_size);
	new_node->next = (*head_ref);

	// Copy the content of new_data to newly allocated memory
	int i;
	for(i = 0; i < data_size; i++)
	{
		*(char *)(new_node->data + i) = *(char *)(new_data + i);
	}

	// Change head pointer as new node is added at the beginning
	(*head_ref) = new_node;
}

/* Function to print nodes in a given linked. fptr is used 
 * to access the function to be used for printing current node data.
 * Note that different data types need different specifier in printf()
 */
void printList(node_t *node, void (*fptr)(void *))
{
	while(node != NULL)
	{
		(*fptr)(node->data);
		node = node->next;
	}
}

// Function to print an integer
void printInt(void *n)
{
	printf(" %d", *(int *)n);
}

// Function to print a float
void printFloat(void *f)
{
	printf(" %f", *(float *)f);
}

// Function to print a char
void printChar(void *c)
{
	printf(" %c", *(char *)c);
}
/* Driver program to test above fuctions */
int main()
{
	node_t *start = NULL;

	// Create and print int linked list
	unsigned int_size = sizeof(int);
	int arr[] = {10, 20, 30, 40, 50};
	int i;
	system("clear");
	for(i = 4; i >= 0; i--)
	{
		push(&start, &arr[i], int_size);
	}
	printf("Created integer linked list is \n");
	printList(start, printInt);
	
	// Create and print float linked list
	unsigned float_size = sizeof(float);
	start = NULL;
	float arr2[] = {10.1, 20.2,30.3, 40.4, 50.5};
	for(i = 4; i >= 0; i--)
	{
		push(&start, &arr2[i], float_size);
	}
	printf("\nCreated float linked list is \n");
	printList(start, printFloat);

	// Create and print float linked list
	unsigned char_size = sizeof(char);
	start = NULL;
	char arr3[] = {'A', 'B', 'C', 'D', 'F'};
	for(i = 4; i >= 0; i--)
	{
		push(&start, &arr3[i], char_size);
	}
	printf("\nCreated char linked list is \n");
	printList(start, printChar);

	printf("\n")	;
	return (0);
}
