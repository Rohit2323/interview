#include <stdio.h>
#include <math.h>

int binary_to_decimal(int n);

int main()
{
	int n;
	printf("Enter a binary number : ");
	scanf("%d", &n);
	printf("%d in binary = %d in decimal\n", n, binary_to_decimal(n));
	return (0);
}

int binary_to_decimal(int n)
{
	int decimalNumber = 0, i = 0, reminder;

	while(n != 0)
	{
		reminder = n % 10;
		n /= 10;
		decimalNumber += reminder * pow(2, i);
		i++;
	}
	return decimalNumber;
}

