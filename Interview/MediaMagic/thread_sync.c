/* Generate 2 threads one will print even nos and other will odd no
* Output should be like : 2 3 4 5 6 ... 99
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
volatile int i = 2;
void *even(void *param);
void *odd(void *param);


int main()
{
	pthread_t ti1, ti2;
	int iret1, iret2;

    while(i < 100)
	{
	iret1 = pthread_create(&ti1, NULL, even, NULL);
	if(iret1)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret1);
		exit(EXIT_FAILURE);
	}
	
	iret2 = pthread_create(&ti2, NULL, odd, NULL);
	if(iret2)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret2);
		exit(EXIT_FAILURE);
	}
	
	pthread_join(ti1, NULL);
	pthread_join(ti2, NULL);
	}
	return(0);
}

void *even(void *param)
{
	pthread_mutex_lock(&mutex1);
	
	if((i != 100) && (i%2 == 0))
	{
		printf("E : %d\n", i);
	}
	i++;
//	printf("Ei = %d\n", i);
	pthread_mutex_unlock(&mutex1);
}

void *odd(void *param)
{
	pthread_mutex_lock(&mutex1);
	if((i != 100) && (i%2 != 0))
	{
		printf("O : %d\n", i);
	}
	i++;
//	printf("Oi = %d\n", i);
	pthread_mutex_unlock(&mutex1);
}	

