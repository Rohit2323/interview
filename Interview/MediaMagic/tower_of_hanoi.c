/* Tower of Hanoi using recursion */

#include <stdio.h>

void tower_of_hanoi(int number, char fromTower, char toTower, char auxTower);

int main()
{
	int number;

	system("clear");
	printf("Enter the number of disks : ");
	scanf("%d", &number);
	printf("The sequence of moves involved in the Tower of Hanoi are :\n");
	tower_of_hanoi(number, 'A', 'B', 'C');
	
	return (0);
}

void tower_of_hanoi(int number, char fromTower, char toTower, char auxTower)
{
	if(number == 1)
	{
		printf("Move disk 1 from tower %c to tower %c\n", fromTower, toTower);
		return;
	}
	tower_of_hanoi(number-1, fromTower, auxTower, toTower);
	printf("Move disk %d from tower %c to tower %c\n", number, fromTower, toTower);
	tower_of_hanoi(number-1, auxTower, toTower, fromTower);
}
