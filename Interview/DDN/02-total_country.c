#include <stdio.h>
#include <stdlib.h>

int solution(int **A, int N, int M) {
    // write your code in C99 (gcc 6.2.0)
	int color = 0;
	int i, j;

	for (i = 0; i < N; i++) {
		for (j = 0; j < M; j++) {
			printf("[%d %d]\n", i, j);
			if (i >= 0 && j < M-1 && i < N-1 && (A[i][j] == A[i][j+1]) && (A[i][j] == A[i+1][j])) {
				printf("(%d, %d) %d\n", i, j, A[i][j]);
				color++;
			} else if (i >= 0 && j < M-1 && i < N-1 && (A[i][j] == A[i][j+1]) && (A[i][j] != A[i+1][j])) {
				printf("(%d, %d) %d\n", i, j, A[i][j]);
				color++;
			} else if (i >= 0 && j < M-1 && i < N-1 && (A[i][j] != A[i][j+1]) && (A[i][j] == A[i+1][j])) {
				printf("(%d, %d) %d\n", i, j, A[i][j]);
				color++;
			} else if (i >= 0 && j < M-1 && i < N-1 && (A[i][j] != A[i][j+1]) && (A[i][j] != A[i+1][j])) {
				printf("(%d, %d) %d\n", i, j, A[i][j]);
				color++;
			}
		}
	}

	return color;
}

int main()
{
	//int A[7][3];

	int **A = (int **)malloc(7 * sizeof(int *));
	for (int i = 0; i < 7; i++) {
		A[i] = (int *)malloc(3 * sizeof(int));
	}

	A[0][0] = 5, A[0][1] = 4, A[0][2] = 4;
	A[1][0] = 4, A[1][1] = 3, A[1][2] = 4;
	A[2][0] = 3, A[2][1] = 2, A[2][2] = 4;
	A[3][0] = 2, A[3][1] = 2, A[3][2] = 2;
	A[4][0] = 3, A[4][1] = 3, A[4][2] = 4;
	A[5][0] = 1, A[5][1] = 4, A[5][2] = 4;
	A[6][0] = 4, A[6][1] = 1, A[6][2] = 1;
	printf("%d\n", solution(A, 7, 3));
	return 0;
}
