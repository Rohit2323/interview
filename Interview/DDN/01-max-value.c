#include <stdio.h>
#include <math.h>

int digit_cnt(int n)
{
	int cnt = 0;

	if (n == 0) {
		cnt = 1;
		return cnt;
	}

	if (n < 0) {
		n = -n;
	}

	while (n > 0) {
		n  = n / 10;
		cnt++;
	}
	return cnt;
}


int solution(int N) {
    // write your code in C99 (gcc 6.2.0)
	int total_digit;
	int last_digit;

	total_digit = digit_cnt(N);

	if (N < 0) {
		N = N - 5 * pow(10, total_digit);
	} else if (N == 0) {
		N = 50;
	} else if (N < 500)  {
		N = N + 5000;
	} else if (N > 500) {
		last_digit = N % 10;
		if (last_digit < 5) {
			N = N * 10 + 50;
		} else {
			N = N * 10 + 5;
		}
	}
	return N;
}

int main()
{
	int N;

	N = 268;
	printf("%d %d\n", N, solution(N));
	
	N = 670;
	printf("%d %d\n", N, solution(N));
	
	N = 0;
	printf("%d %d\n", N, solution(N));
	
	N = -999;
	printf("%d %d\n", N, solution(N));

	return 0;
}
