#include <stdio.h>
#include <math.h>

int digit_cnt(int n)
{
	int cnt = 0;

	if (n == 0) {
		cnt = 1;
		return cnt;
	}

	if (n < 0) {
		n = -n;
	}

	while (n > 0) {
		n  = n / 10;
		cnt++;
	}
	return cnt;
}

int get_digit(int n, int digit)
{
	int result = 0;

	while (digit > 0) {
		result = n % 10;
		n = n/10;
		digit--;
	}
	return result;
}

#if 1
int solution(int N) {
    // write your code in C99 (gcc 6.2.0)
	int total_digit;
	int last_digit;

	total_digit = digit_cnt(N);

	if (N < 0) {
		N = N - 5 * pow(10, total_digit);
	} else if (N == 0) {
		N = 50;
	} else if (N < 5 * pow(10, total_digit-1))  {
		N = N + 5 * pow(10, total_digit);
	} else if (N > 5 * pow(10, total_digit-1)) {
		last_digit = N % 10;
		if (last_digit < 5) {
			N = N * 10 + 50;
		} else {
			N = N * 10 + 5;
		}
	}
	return N;
}
#else
int solution(int N) {
    // write your code in C99 (gcc 6.2.0)
	int total_digit;
	int last_digit;

	total_digit = digit_cnt(N);

	if (N < 0) {
		N = N - 5 * pow(10, total_digit);
	} else if (N == 0) {
		N = 50;
	} else if (N < 5 * pow(10, total_digit-1))  {
		N = N + 5 * pow(10, total_digit);
	} else if (N > 5 * pow(10, total_digit-1)) {
		/*last_digit = N % 10;
		if (last_digit < 5) {
			N = N * 10 + 50;
		} else {
			N = N * 10 + 5;
		}*/
		for (int i = total_digit; i > 0; i--) {
			last_digit = get_digit(N, i);
			printf("digit %d\n", last_digit);
			if (last_digit < 5) {
				N = N * 10 + 50;
			} else {
				N = N * 10 + 5;
			}
		}
	}
	return N;
}
#endif

int main()
{
	int N;

	/*
	N = 268;
	printf("%d %d\n", N, solution(N));
	
	N = 4;
	printf("%d %d\n", N, solution(N));
	
	N = 8;
	printf("%d %d\n", N, solution(N));
	
	N = 48;
	printf("%d %d\n", N, solution(N));
	
	N = 670;
	printf("%d %d\n", N, solution(N));
	
	N = 68;
	printf("%d %d\n", N, solution(N));
	
	N = 6706;
	printf("%d %d\n", N, solution(N));
	
	N = 6406;
	printf("%d %d\n", N, solution(N));
	
	N = 0;
	printf("%d %d\n", N, solution(N));
	
	N = -999;
	printf("%d %d\n", N, solution(N));
	*/
	N = 6406;
	printf("%d %d\n", N, solution(N));
	return 0;
}
