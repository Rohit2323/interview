#include <stdio.h>

int solution(int A[], int N) {
    // write your code in C99 (gcc 6.2.0)
	int hill = 0;
	int valley = 0;
	int i = 0;
	int p, q;
	int p1 = 0, q1 = 0;
	
	while (i < N) {
		p = A[i];
		if (i < N-1) {
			q = A[i+1];
		} else {
			q = 0;
		}
		if (i > 0) {
			p1 = A[i-1];
		}
		
		if (i < N-2) {
			q1 = A[i+2];
		}
		printf("%d %d %d %d\n", p1, p, q, q1);

		if ((p1 < p) && (q1 <= q)) {
			printf("1. %d %d %d %d\n", p1, p, q, q1);
			hill++;
		}
		
		if ((p1 > p)  && (q1 > q)) {
			printf("2. %d %d %d %d\n", p1, p, q, q1);
			valley++;
		}
		i++;
	}

	return hill+valley;
}

int main()
{
	//int A[] = {2, 2, 3, 4, 3, 3, 2, 2, 1, 1, 2, 5};
	int A[] = {-3, -3};

	printf("%d\n", solution(A, 2));
	return 0;
}
