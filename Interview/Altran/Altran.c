Server.c

int sockfd;
struct sockadd_in server;
char buffer[1024];

if ((sockfd = socket(AF_INET, SOCK_STREAM, 0) < 0)
{
	printf("Failed to create socket");
	return 0;
}

memset(&server, 0, sizeof(server));

server.sin_family = AF_FAMILY;
server.sin_addr.s_addr = INADDR_ANY;
server.sin_port = 8080;

if ((bind(sockfd, (const sockaddr*)&server, sizeof(sockaddr)) < 0)
{
	printf("Failed to bind socket");
	return 0;
}

if ((listen(sockfd, 128)) < 0) {
	printf("Failed to listen");
	return 0;
}

int new_sockfd = accept(sockfd, (const sockaddr*)&server, sizeof(sockaddr)));

read(new_sockfd, buffer, 1024);

send(new_sockfd, buffer, 1024, 0);


------------------------------------------------------------------------------------------------------------
Client.c
int sockfd;

struct sockadd_in server;
char buffer[1024];


if ((sockfd = socket(AF_INET, SOCK_STREAM, 0) < 0)
{
	printf("Failed to create socket");
}

memset(&server, 0, sizeof(server));

server.sin_family = AF_FAMILY;
server.sin_addr.s_addr = INADDR_ANY;
server.sin_port = 8080;


connect(sockfd, (const sockaddr*)&server, sizeof(sockaddr)));

strcpy(buffer, "Hello from client");
send(sockfd, buffer, 1024, 0);

read(sockfd, buffer, 1024);


