#include <stdio.h>
#include <string.h>
#include <stdint.h>

// Program to write own memcpy() with 4 byte memory alignment

void mem_cpy(void *dest, void *src, int size)
{
	uint8_t *pdest = (uint8_t *)dest;
	uint8_t *psrc = (uint8_t *)src;

	int loops = (size/sizeof(uint32_t));
	printf("%d loops %d\n", size, loops);
	for (int i = 0; i < loops; i++) {
		*((uint32_t *)pdest) = *((uint32_t *)psrc);
		pdest += sizeof(uint32_t);
		psrc += sizeof(uint32_t);
		printf("1\n");
	}
	
	loops = (size % sizeof(uint32_t));
	printf("loops %d\n", loops);
	for (int i = 0; i < loops; i++) {
		*pdest = *psrc;
		++pdest;
		++psrc;
		printf("2\n");
	}
}

int main()
{
	char *src = "Hi qwerty asdfg zxcv qwerty asdfgh zxcv 1234567890";
	char dest[100];
	mem_cpy(dest, src, strlen(src)+1);
	printf("DEST : %s\n", dest);
	return 0;
}
