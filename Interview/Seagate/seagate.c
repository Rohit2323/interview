#include<stdio.h>
#include<stdlib.h>

void positive_case();
void error_all();
void error_at_some_place();

// error code if error print -1
int main()
{
	system("clear");
	positive_case();
	printf("--------------------------------------------------------\n");
	error_all();
	printf("--------------------------------------------------------\n");
	error_at_some_place();
	printf("--------------------------------------------------------\n");
/*
	int TL[5] = {1, 2, 3, 4, 5};
	int PL[5] = {4, 3, 0, 5, 2};
	//int PL[5] = {4, 1, 5, 0, 2};
	int SL[5] = {};
	int flag = 0;
	int cnt = 0;
	//output int SL[5] = {3, 2, 5, 4, 1};
	//output int SL[5] = {4, 1, 2, 5, 3};
	int i = 0,j = 0,k = 0,l = 0;
	int track = 0;
	int loop = 0;
	system("clear");
	printf("-1 is nothing but error code in this program\n");
	for(i = 0; i < 5; i++)
	{
//		printf("inside for i = %d\n", i);
		
		if(PL[i] != 0)
		{
			loop++;
			if(loop == 5)
				printf("Error...\n");
		}
		if(PL[i] == 0)
		{
			SL[j] = TL[i];
//			printf("SL[%d] = %d\n", j, SL[j]);
			j++;
			while(k < 5)
			{
				track++;
//				printf("Inside while k = %d\n", k);
				if(PL[k] == TL[i])
				{
					cnt++;
//					printf("TL[%d] = %d\n", k, TL[k]);
					SL[j] = TL[k];
//					printf("SL[%d] = %d\n", j, SL[j]);
					j++;
					flag = 1;
					if(cnt == 4)
					{
						break;
					}
					track = 0;
				}
				if((flag == 1) && (i < 5))
				{
					i = k;
					k = 0;
					flag = 0;
				}
				else
				{
					k++;
				}
				if(track == 5)
				{
					SL[j] = -1;
//					printf("SL[%d] = %d\n", j, SL[j]);
				}
			}
			if(track != 5)
			{
				for(l = 0; l < 5; l++)
				{
					printf("SL[%d] = %d\n", l, SL[l]);
				}
			}
			else
			{
				for(l = 0; l < j+1; l++)
				{
					printf("SL[%d] = %d\n", l, SL[l]);
				}
			}
			i = 6;
		}
	}
*/
	return 0;
}


void positive_case()
{
	int TL[5] = {1, 2, 3, 4, 5};
	int PL[5] = {4, 3, 0, 5, 2};
	//int PL[5] = {4, 1, 5, 0, 2};
	int SL[5] = {};
	int flag = 0;
	int cnt = 0;
	//output int SL[5] = {4, 1, 2, 5, 3};
	int i = 0,j = 0,k = 0,l = 0;
	int track = 0;
	int loop = 0;
	printf("\npositive case\n");
	printf("-1 is nothing but error code in this program\n");
	printf("TL[5] = ");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", TL[i]);
	}
	printf("\n");
	printf("PL[5] = ");
	for(i =	0; i < 5; i++)
	{
		printf("%d ", PL[i]);
	}
	printf("\n");
	for(i = 0; i < 5; i++)
	{
		
		if(PL[i] != 0)
		{
			loop++;
			if(loop == 5)
				printf("Error...\n");
		}
		if(PL[i] == 0)
		{
			SL[j] = TL[i];
			j++;
			while(k < 5)
			{
				track++;
				if(PL[k] == TL[i])
				{
					cnt++;
					SL[j] = TL[k];
					j++;
					flag = 1;
					if(cnt == 4)
					{
						break;
					}
					track = 0;
				}
				if((flag == 1) && (i < 5))
				{
					i = k;
					k = 0;
					flag = 0;
				}
				else
				{
					k++;
				}
				if(track == 5)
				{
					SL[j] = -1;
				}
			}

			if(track != 5)
			{
				printf("SL[5] = ");
				for(l = 0; l < 5; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			else
			{
				printf("SL[5] = ");
				for(l = 0; l < j+1; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			i = 6;
		}
	}
}


void error_all()
{
	int TL[5] = {1, 2, 3, 4, 5};
	int PL[5] = {4, 3, 7, 5, 2};
	int SL[5] = {};
	int flag = 0;
	int cnt = 0;
	//output Error...;
	int i = 0,j = 0,k = 0,l = 0;
	int track = 0;
	int loop = 0;
	printf("error all case\n");
	printf("-1 is nothing but error code in this program\n");
	printf("TL[5] = ");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", TL[i]);
	}
	printf("\n");
	printf("PL[5] = ");
	for(i =	0; i < 5; i++)
	{
		printf("%d ", PL[i]);
	}
	printf("\n");
	for(i = 0; i < 5; i++)
	{
		if(PL[i] != 0)
		{
			loop++;
			if(loop == 5)
				printf("Error...\n");
		}
		if(PL[i] == 0)
		{
			SL[j] = TL[i];
			j++;
			while(k < 5)
			{
				track++;
				if(PL[k] == TL[i])
				{
					cnt++;
					SL[j] = TL[k];
					j++;
					flag = 1;
					if(cnt == 4)
					{
						break;
					}
					track = 0;
				}
				if((flag == 1) && (i < 5))
				{
					i = k;
					k = 0;
					flag = 0;
				}
				else
				{
					k++;
				}
				if(track == 5)
				{
					SL[j] = -1;
				}
			}
			if(track != 5)
			{
				printf("SL[5] = ");
				for(l = 0; l < 5; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			else
			{
				printf("SL[5] = ");
				for(l = 0; l < j+1; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			i = 6;
		}
	}
}

void error_at_some_place()
{
	int TL[5] = {1, 2, 3, 4, 5};
	int PL[5] = {4, 3, 0, 6, 2};
	int SL[5] = {};
	int flag = 0;
	int cnt = 0;
	//output int SL[5] = {3, 2, 5, error};
	int i = 0,j = 0,k = 0,l = 0;
	int track = 0;
	int loop = 0;
	printf("error at some place\n");
	printf("-1 is nothing but error code in this program\n");
	printf("TL[5] = ");
	for(i = 0; i < 5; i++)
	{
		printf("%d ", TL[i]);
	}
	printf("\n");
	printf("PL[5] = ");
	for(i =	0; i < 5; i++)
	{
		printf("%d ", PL[i]);
	}
	printf("\n");

	for(i = 0; i < 5; i++)
	{
		if(PL[i] != 0)
		{
			loop++;
			if(loop == 5)
				printf("Error...\n");
		}
		if(PL[i] == 0)
		{
			SL[j] = TL[i];
			j++;
			while(k < 5)
			{
				track++;
				if(PL[k] == TL[i])
				{
					cnt++;
					SL[j] = TL[k];
					j++;
					flag = 1;
					if(cnt == 4)
					{
						break;
					}
					track = 0;
				}
				if((flag == 1) && (i < 5))
				{
					i = k;
					k = 0;
					flag = 0;
				}
				else
				{
					k++;
				}
				if(track == 5)
				{
					SL[j] = -1;
				}
			}
			if(track != 5)
			{
				printf("SL[5] = ");
				for(l = 0; l < 5; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			else
			{
				printf("SL[5] = ");
				for(l = 0; l < j+1; l++)
				{
					printf("%d ", SL[l]);
				}
				printf("\n");
			}
			i = 6;
		}
	}
}
