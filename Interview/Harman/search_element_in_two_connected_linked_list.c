/* Program to search element in given 2 connected linked list 
 *e.g L1 : 1 2 3 4 5 6 7
 *    L2 : 11 12 6 7
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct list
{
	int data;
	struct list *next;
}node_t;

node_t *head1 = NULL;
node_t *head2 = NULL;
node_t *current1;
node_t *current2;
int flag = 0;
int flag1 = 0;

node_t* create_node(int value)
{
	node_t *new_node;
	new_node = (node_t*)malloc(sizeof(node_t));
	if(new_node == NULL)
	{
		printf("Failed to create new node\n");
	}
	new_node->data = value;
	new_node->next = NULL;

	return new_node;
}

void display_list1()
{
	node_t *trav1 = head1;
	printf("Link list1 : ");
	while(trav1 != NULL)
	{
		printf("%d->", trav1->data);
		trav1 = trav1->next;
	}
	printf("NULL\n");
}

void display_list2()
{
	node_t *trav2 = head2;
	printf("Link list2 : ");
	while(trav2 != NULL)
	{
		printf("%d->", trav2->data);
		trav2 = trav2->next;
	}
	printf("NULL\n");
}

void display_list(int list_no)
{
	if(list_no == 1)
	{
		display_list1();
	}
	else if(list_no == 2)
	{
		display_list2();
	}
	else if(list_no == 3)
	{

		display_list1();
		display_list2();
	}
}

void search_node(int element)
{
	node_t *trav1 = head1;
	node_t *trav2 = head2;

	flag = 0;
	while(trav1 != NULL)
	{
		if(trav1->data == element)
		{
			printf("Element %d found in list\n", element);
			flag = 1;
			return;
		}
		trav1 = trav1->next;
	}
	while((trav2 != NULL) && (flag == 0))
	{
		if(trav2->data == element)
		{
			printf("Element %d found in list\n", element);
			flag = 1;
			return;
		}
		trav2 = trav2->next;
	}
	if(flag == 0)
	{
		printf("Element %d not found in list\n", element);
	}
	
}
void add_last(node_t *new_node, int list_no)
{
	if((list_no == 1) && (flag == 0))
	{
		if(head1 == NULL)
		{
			head1 = new_node;
			current1 = new_node;
		}
		else
		{
			current1->next = new_node;
			current1 = new_node;
		}
	}
	else if((list_no == 2) && (flag == 0))
	{
		if(head2 == NULL)
		{
			head2 = new_node;
			current2 = new_node;
		}
		else
		{
			current2->next = new_node;
			current2 = new_node;
		}
	}
	else if((list_no == 3)||(flag == 1))
	{
		flag = 1;
		if((head1 == NULL) && (head2 == NULL))
		{
			head1 = new_node;
			current1 = new_node;
			head2 = new_node;
			current2 = new_node;
		}
		else
		{
			if(head1 == NULL)
			{
				head1 = new_node;
				current1 = new_node;
			}
			else
			{
				current1->next = new_node;
				current1 = new_node;
			}
			if(head2 == NULL)
			{
				head2 = new_node;
				current2 = new_node;
			}
			else
			{
				current2->next = new_node;
				current2 = new_node;
			}
		}
	}
}

int main()
{
	system("clear");
	int ch, n, list_no, list;
	int element;
		
	printf("1: Add_last\n2: Display\n3: Search\n5: Exit\n");
	do
	{
		printf("Enter your choice : ");
		scanf("%d", &ch);
		switch(ch)
		{
			case 1: 
				printf("Enter no to add to list : ");
				scanf("%d", &n);
				printf("1: list1\n2: list2\n3: both\n");
				printf("Enter the link list no to which %d will add : ", n);
				scanf("%d", &list_no);
				add_last(create_node(n), list_no);
				break;

			case 2:
				printf("1: list1\n2: list2\n3: both\n");
				printf("Enter the link list no to display : ");
				scanf("%d", &list);
				display_list(list);
				break;

			case 3:
				printf("Enter the element to search in the list: ");
				scanf("%d", &element);
				search_node(element);
				break;

			default :
				break;
		}

	}while(ch != 5);
	return (0);
}
