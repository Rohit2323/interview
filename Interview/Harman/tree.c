#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int data;
	struct node *leftChild;
	struct node *rightChild;
}node_t;

node_t *root = NULL;

void insert(int data)
{
	node_t *current;
	node_t *parent;
	node_t *tempNode = (node_t*)malloc(sizeof(node_t));
	if(tempNode == NULL)
	{
		printf("Failed to allocate memory\n");
		exit;
	}
	tempNode->data = data;
	tempNode->leftChild = NULL;
	tempNode->rightChild = NULL;

	// if tree is empty
	if(root == NULL)
	{
		root = tempNode;
	}
	else
	{
		current = root;
		parent = NULL;

		while(1)
		{
			parent = current;
	
			// go to left of the tree
			if(data < parent->data)
			{
				current = current->leftChild;
	
				// insert to left
				if(current == NULL)
				{
					parent->leftChild = tempNode;
					return;
				}
			}
			// go to right of the tree
			else
			{
				current = current->rightChild;

				// insert to right
				if(current == NULL)
				{
					parent->rightChild = tempNode;
					return;
				}
			}
		}
	}
}

void pre_order_traversal(node_t *root)
{
	if(root != NULL)
	{
		printf("%d ", root->data);
		pre_order_traversal(root->leftChild);
		pre_order_traversal(root->rightChild);
	}
}

void in_order_traversal(node_t *root)
{
	if(root != NULL)
	{
		in_order_traversal(root->leftChild);
		printf("%d ", root->data);
		in_order_traversal(root->rightChild);
	}
}

void post_order_traversal(node_t *root)
{
	if(root != NULL)
	{
		post_order_traversal(root->leftChild);
		post_order_traversal(root->rightChild);
		printf("%d ", root->data);
	}
}

int main()
{
	int i;
	int array[7] = {27, 14, 35, 10, 19, 31, 42};

	system("clear");

	for(i = 0; i < 7; i++)
	{
		insert(array[i]);
	}

	printf("\nPreorder traversal: ");
	pre_order_traversal(root);

	printf("\nInorder traversal: ");
	in_order_traversal(root);
	
	printf("\nPostorder traversal: ");
	post_order_traversal(root);

	printf("\n");
	return (0);
}
