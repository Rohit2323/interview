#include <stdio.h>

int main()
{
	int x = 10;

	printf("%d\n", sizeof(x++)); // 4
	printf("%d\n", x); // 10

	return 0;
}
