#include <stdio.h>

int solution(int *A, int N, int X) {
    int r, m, l;
    if (N == 0) {
        return -1;
    }
    l = 0;
    r = N - 1;
    while (l <= r) {
        m = (l + r) / 2;
        if (A[m] > X)  {
            r = m - 1;
        } else {
           if (A[l] == X) break;
            l = m;
        }
    }
    if (A[l] == X) {
        return l;
    }
    return -1;
}

int main()
{
	//int A[] = {1, 2, 5, 9, 9};
	int A[] = {-1, 1, 2, 5, 6, 7, 9};

	int result = solution(A, 7, 7);
	printf("result %d\n", result);
	return 0;
}
