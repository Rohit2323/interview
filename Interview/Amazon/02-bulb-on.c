#include <stdio.h>

int solution(int A[], int N) {
    // write your code in C99 (gcc 6.2.0)
	int H[101] = {0};
	int i, j;
	int count = 0;
	int flag = 0;

	for (i = 0; i < N; i++) {
		H[A[i]]++;
		flag = 0;
		for (j = 1; j < A[i]; j++) {
			if (H[j] != 1) {
				flag = 1;
				printf("%d\n", A[i]);
				break;
			}
		}
		if (flag == 0) {
			printf("%d\n", A[i]);
			count++;
		}
	}
	return count;
}

int main()
{
	//int A[] = {2, 1, 3, 5, 4}; // o/p = 3
	//int A[] = {2, 3, 4, 1, 5}; // o/p = 2
	int A[] = {1, 3, 4, 2, 5}; // o/p = 3

	int result = solution(A, 5);
	printf("Result %d\n", result);
	return 0;
}
