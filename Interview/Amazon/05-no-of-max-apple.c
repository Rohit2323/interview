#include <stdio.h>

int solution(int A[], int N, int K, int L) {
    // write your code in C99 (gcc 6.2.0)
	if (K+L > N) {
		return -1;
	}

	int i;
	int j;
	int max_alice = 0;
	int max_bob = 0;
	int k, l;
	int prev_alice = 0;
	int prev_bob = 0;

	for (i = 0, j = N-1; i < j; i++, j--) {
		if (prev_alice < max_alice) {
			prev_alice = max_alice;
		}

		if (prev_bob < max_bob) {
			prev_bob = max_bob;
		}

		max_alice = 0;
		max_bob = 0;
		k = i;
		//printf("i %d %d\n", k, K+i);
		while (k < K+i) {
			max_alice += A[k];
			k++;
		}

		l = j;
		//printf("l %d %d\n", l, N-L-1-i);
		while ((l > k) && (l > N-L-1-i)) {
			max_bob += A[l];
			l--;
		}
	}
	return prev_alice+prev_bob;

}

int main()
{
	/* // O/P = 24 = 13+11
	int A[] = {6, 1, 4, 6, 3, 2, 7, 4};
	int K = 3, L = 2;

	int result = solution(A, 8, K, L);*/

	// O/P = -1
	int A[] = {10, 19, 15};
	int K = 2, L = 2;

	int result = solution(A, 3, K, L);
	printf("Result %d\n", result);
	return 0;
}
