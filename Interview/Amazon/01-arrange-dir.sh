#!/bin/bash

mkdir music
mkdir images
mkdir videos

ls -1 > tmp.me
#cat tmp.me | while read line
for line in `ls -1`
do
	is_music=`echo $line | grep -c ".mp3\|.flac"`
	if [ $is_music -ge 1 ]
	then
		mv $line music/
	fi
	
	is_images=`echo $line | grep -c ".jpg\|.png"`
	if [ $is_images -ge 1 ]
	then
		mv $line images/
	fi
	
	is_videos=`echo $line | grep -c ".avi\|.mov"`
	if [ $is_videos -ge 1 ]
	then
		mv $line videos/
	fi

done

rm *.log

