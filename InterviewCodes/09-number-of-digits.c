#include <stdio.h>
#include <math.h>

// Program to find out number of digits in a given number
int method1_recursion(int n)
{
	if (n == 0) {
		return 0;
	}
	while (n != 0) {
		return 1 + method1_recursion(n/10);
	}
}

int method2_log(int n)
{
	if (n < 0) {
		n = -n;
	}
	return (floor(log10(n)+1));
}

int method3_digit(int n)
{
	int digit = 0;

	if (n == 0) {
		digit = 1;
	}

	while (n != 0) {
		n = n /10;
		digit++;
	}
	return digit;
}

int main()
{
	int no;

	printf("Enter the number : ");
	scanf("%d", &no);

	printf("%d contains %d digits\n", no, method1_recursion(no));
	printf("%d contains %d digits\n", no, method2_log(no));
	printf("%d contains %d digits\n", no, method3_digit(no));
	return 0;
}
