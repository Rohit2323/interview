#include <stdio.h>

int main()
{
	char A = 'A';
	char B = 'B';
	
	printf("Before Swap A: %c B: %c\n", A, B);
	A = A ^ B;
	B = A ^ B;
	A = A ^ B;

	printf("After Swap A: %c B: %c\n", A, B);
}
