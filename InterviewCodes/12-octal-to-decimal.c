#include <stdio.h>
#include <math.h>

int octal_to_decimal(int n)
{
	int decimal = 0, i = 0, reminder;

	while (n != 0) {
		reminder = n % 10;
		n = n/10;
		decimal += reminder * pow(8, i);
		i++;
	}
	return decimal;
}

int main()
{
	int bin;

	printf("Enter octal number to be converted to decimal : ");
	scanf("%d", &bin);
	printf("%d : %d\n", bin, octal_to_decimal(bin));

	return 0;
}
