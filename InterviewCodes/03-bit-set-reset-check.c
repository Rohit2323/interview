#include <stdio.h>

// Program to check whether bit is set/reset

#define BIT_SET_CHECK(number, bit) (number & 1 << bit)
#define BIT_SET(number, bit) (number | 1 << bit)
#define BIT_RESET(number, bit) (number & ~(1 << bit))

int main()
{
	int number, bit;

	printf("Enter number : ");
	scanf("%d", &number);

	printf("Enter bit to check whether set or not : ");
	scanf("%d", &bit);

	if (BIT_SET_CHECK(number, bit)) {
		printf("Bit %d is 1 in %d\n", bit, number);
	} else {
		printf("Bit %d is 0 in %d\n", bit, number);
	}
	
	printf("Bit %d is SET = %d\n", bit, BIT_SET(0, bit));
	printf("Bit %d is RESET = %d\n", bit, BIT_RESET(number, bit));

	return 0;
}
