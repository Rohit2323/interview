#include <stdio.h>

// Program to swap nibbles
// nibble means 4 bits

int main()
{
	int number = 159; // o/p = 249

	number = (number & 0x0f) << 4 | (number & 0xf0) >> 4;
	printf("%d\n", number);

	return 0;
}
