#include <stdio.h>

// Program to convert given decimal number into binary

int decimal_to_binary(int decimal)
{
	int binary = 0;
	int rem;
	int i = 1;

	while (decimal > 0) {
		rem = decimal % 2;
		binary = binary + rem * i;
		i = i * 10;
		decimal = decimal / 2;
	}

	return binary;
}

int main()
{
	int no;

	printf("Enter the number: ");
	scanf("%d", &no);

	printf("Binary equivalent %d of %d decimal\n", decimal_to_binary(no), no);

	return 0;
}
