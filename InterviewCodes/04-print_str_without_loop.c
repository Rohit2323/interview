#include <stdio.h>

void print_string(char *str)
{
	static int c = 100;

	if (c != 0) {
		c--;
		printf("%d : %s\n", c, str);
		print_string(str);
	}
}


int main()
{
	print_string("Hello World");

	return 0;
}
