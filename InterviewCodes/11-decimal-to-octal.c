#include <stdio.h>

// Program to convert given decimal number into octal

int decimal_to_octal(int decimal)
{
	int octal = 0;
	int rem;
	int i = 1;

	while (decimal > 0) {
		rem = decimal % 8;
		octal = octal + rem * i;
		i = i * 10;
		decimal = decimal / 8;
	}

	return octal;
}

int main()
{
	int no;

	printf("Enter the number: ");
	scanf("%d", &no);

	printf("Octal equivalent %d of %d decimal\n", decimal_to_octal(no), no);

	return 0;
}
