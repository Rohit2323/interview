#include <stdio.h>

char *str = "Rohit";

void f1()
{
	*str = "Rohit";
	printf("%s\n", str);
}

int main()
{
	f1();

	char *str = "Anurag";
	printf("%s\n", str);
	return 0;
}
