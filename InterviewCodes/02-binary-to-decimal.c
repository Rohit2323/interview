#include <stdio.h>

int binary_to_decimal(int n)
{
	int decimal = 0, i = 0, reminder;

	while (n != 0) {
		reminder = n % 10;
		n = n/10;
		decimal += reminder * (1 << i);
		//decimal += reminder * pow(2, i);
		i++;
	}
	return decimal;
}

int main()
{
	int bin;

	printf("Enter binary number to be converted to decimal : ");
	scanf("%d", &bin);
	printf("%d : %d\n", bin, binary_to_decimal(bin));

	return 0;
}
