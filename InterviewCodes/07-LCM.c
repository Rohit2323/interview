#include <stdio.h>

// Program to find LCM of 2 numbers

int main()
{
	int a, b, lcm;
	printf("Enter 2 numbers : ");
	scanf("%d %d", &a, &b);

	lcm = a > b? a : b; // max

	while (1) {
		if ((lcm % a == 0) && (lcm % b == 0)) {
			printf("\nLCM of %d and %d is %d\n", a, b, lcm);
			break;
		}
		++lcm;
	}

	return 0;
}
