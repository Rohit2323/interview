#include <stdio.h>

// Program to find HCF(Highest Common Factor) or GCD(Greatest Common Divisor)

int main()
{
	int a, b, small, gcd, i;

	printf("Enter 2 numbers : ");
	scanf("%d %d", &a, &b);

	small = a > b? b : a; // min

	for (i = small; i >= 1; i--) {
		if ((a % i == 0) && (b % i == 0)) {
			gcd = i;
			printf("GCD of %d and %d is %d\n", a, b, gcd);
			break;
		}
	}
	return 0;
}
