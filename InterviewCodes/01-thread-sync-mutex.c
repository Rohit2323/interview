/* Generate 2 threads one will print even nos and other will odd no
* Output should be like : 1 2 3 4 5 6 ... 100
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define COUNT_DONE 100

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

volatile int i = 1;

void *even(void *arg);
void *odd(void *arg);

int main()
{
	pthread_t ti1, ti2;
	int iret1, iret2;

	iret1 = pthread_create(&ti1, NULL, even, NULL);
	if(iret1)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret1);
		exit(EXIT_FAILURE);
	}
	
	iret2 = pthread_create(&ti2, NULL, odd, NULL);
	if(iret2)
	{
		fprintf(stderr, "Error - pthread_create() return code: %d\n", iret2);
		exit(EXIT_FAILURE);
	}
	
	pthread_join(ti1, NULL);
	pthread_join(ti2, NULL);
	
	return(0);
}

/* Method1 : mutex and conditional variable */
#if 0
void *even(void *arg)
{
	while (1) {
		pthread_mutex_lock(&mutex);
		if (i % 2 == 0) {
			pthread_cond_wait(&cond, &mutex);
		}
		i++;
		printf("E : %d\n", i);
		pthread_cond_signal(&cond);
		if (i >= COUNT_DONE-1) {
			pthread_mutex_unlock(&mutex);
			return NULL;
		}
		pthread_mutex_unlock(&mutex);
	}
}

void *odd(void *arg)
{
	while (1) {
		pthread_mutex_lock(&mutex);
		if (i % 2 != 0) {
			pthread_cond_wait(&cond, &mutex);
		}
		i++;
		printf("O : %d\n", i);
		pthread_cond_signal(&cond);
		if (i >= COUNT_DONE-1) {
			pthread_mutex_unlock(&mutex);
			return NULL;
		}
		pthread_mutex_unlock(&mutex);
	}
}	
#else
/* Method2 : Only mutex */
void *even(void *arg)
{
	pthread_mutex_lock(&mutex);
	do {
		if (i % 2 == 0) {
			printf("E : %d\n", i);
			i++;
		} else {
			pthread_mutex_unlock(&mutex); // if no is odd do not print release mutex
		}
	} while (i <= COUNT_DONE);
}

void *odd(void *arg)
{
	pthread_mutex_lock(&mutex);
	do {
		if (i % 2 != 0) {
			printf("O : %d\n", i);
			i++;
		} else {
			pthread_mutex_unlock(&mutex); // if no is even do not print release mutex
		}
	} while (i <= COUNT_DONE);
}
#endif
